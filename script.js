// Ajout d'un symbole "coché" sur chaque élément de la liste
var list = document.querySelector("ul");
list.addEventListener(
  "click",
  function (ev) {
    if (ev.target.tagName === "LI") {
      ev.target.classList.toggle("checked");
    }
  },
  false
);

// Création d'un nouvel élément lorsque l'on clique sur le bouton "Ajouter"
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  var x = document.getElementById("myUL");
  li.appendChild(t);
  if (inputValue === "") {
    alert("Tu dois écrire quelque chose");
  } else {
    x.appendChild(li);
    x.classList.add("ulPad");
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      var div = this.parentElement;
      div.remove();
      console.log(close.length);
      if (close.length == 0) {
        x.classList.remove("ulPad");
      }
    };
  }
}

// Creation d'un bouton "close" et ajout de du bouton à chaque élément de la liste
var myNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}

// Cacher la tâche en cours en cliquant sur le bouton close
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function () {
    var div = this.parentElement;
    div.remove();
  };
}

// Filtrer par tâches complétées
function filterCompleted() {
  var list = document.getElementById("myUL");
  var completed = list.getElementsByClassName("checked");
  for (var i = 0; i < completed.length; i++) {
    completed[i].style.display = "";
  }
  var notCompleted = list.getElementsByTagName("li");
  for (var i = 0; i < notCompleted.length; i++) {
    if (!notCompleted[i].classList.contains("checked")) {
      notCompleted[i].style.display = "none";
    }
  }
}

// Filtrer par tâches en cours
function filterInProgress() {
  var list = document.getElementById("myUL");
  var notCompleted = list.getElementsByTagName("li");
  for (var i = 0; i < notCompleted.length; i++) {
    if (notCompleted[i].classList.contains("checked")) {
      notCompleted[i].style.display = "none";
    } else {
      notCompleted[i].style.display = "";
    }
  }
}

// Afficher toutes les tâches (complétées et en cours)
function showAll() {
  var list = document.getElementById("myUL");
  var items = list.getElementsByTagName("li");
  for (var i = 0; i < items.length; i++) {
    items[i].style.display = "";
  }
}

// Supprime toutes les tâches
function deleteAll() {
  var list = document.getElementById("myUL");
  var lists = document.querySelectorAll("li").forEach((el) => el.remove());
  list.classList.remove("ulPad");
}

// Exporter toutes les tâches
function exportTasks() {
  var list = document.getElementById("myUL");
  var items = list.getElementsByTagName("li");
  var content = "";
  for (var i = 0; i < items.length; i++) {
    content += items[i].textContent + "\n";
  }
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(content)
  );
  element.setAttribute("download", "tâches.txt");
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}